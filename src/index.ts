import { Action, Reducer, CombinedState } from 'redux';

export const LOGOUT_SUCCESS = 'redux-switch-user/LOGOUT_SUCCESS';
export const LOGIN_SUCCESS = 'redux-switch-user/LOGIN_SUCCESS';
export const SWITCH_USER = 'redux-switch-user/SWITCH_USER';

export const USER_STORE_KEY = 'appusers';

let storage: any = null;

type RS = {
  makeRootReducer: () => Reducer<unknown, Action<any>>;
};

export type UserObj = {
  id: string | number;
  username: string;
  name: string;
};

/**
 * Store user state for later use
 * @param key Storage key
 * @param state Application state
 * @returns {Promise<any>}
 */
export const preserveUserState = async (key: string, state: CombinedState<any>): Promise<boolean> => {
  if (!storage) {
    return false;
  }
  return storage.setItem(`user_data_${key}`, JSON.stringify(state));
};

/**
 * Get stored user state
 * @param key Storage key
 * @returns {Promise<CombinedState<any>>} Stored user state
 */
export const getStoredUserState = async (key: string): Promise<CombinedState<any>> => {
  if (!storage) {
    return {};
  }

  try {
    const data = await storage.getItem(`user_data_${key}`);
    return JSON.parse(data);
  } catch (e) {
    return {};
  }
};

const storeUsers = (users: Array<UserObj>): void => {
  storage.setItem(USER_STORE_KEY, JSON.stringify(users));
};

/**
 * Get saved user entries
 * @returns {Promise<Array<UserObj>>}
 */
export const getStoredUsers = async (): Promise<Array<UserObj>> => {
  if (!storage) {
    return [];
  }

  try {
    const data = await storage.getItem(USER_STORE_KEY);
    return JSON.parse(data);
  } catch (e) {
    return [];
  }
};

export const removeStoredUser = async (id: string | number): Promise<Array<UserObj>> => {
  if (!storage) {
    return [];
  }

  try {
    const data = await storage.getItem(USER_STORE_KEY);
    const unpacked: Array<UserObj> = JSON.parse(data);
    const delInd = unpacked.findIndex((u) => u.id == id);
    if (delInd != -1) {
      unpacked.splice(delInd, 1);
      storeUsers(unpacked);
      await storage.removeItem(`user_data_${id}`);
    }
    return unpacked;
  } catch (e) {
    console.error(e);
    return [];
  }
};

/**
 * @param appReducer  combined App reducer
 * @param initialState inital state object
 * @param storage storage engine used in the app
 * @returns {RS}
 */
const reduxSwitch = (appReducer: any, initialState: any = {}, storageEngine: any): RS => {
  storage = storageEngine;

  const makeRootReducer = () => {
    return (state: ReturnType<typeof appReducer>, action: any) => {
      if (action.type === LOGOUT_SUCCESS) {
        return appReducer(initialState, action);
      }
      if (action.type === LOGIN_SUCCESS) {
        storeUsers(action.payload);
        return appReducer(state, action);
      }

      if (action.type === SWITCH_USER) {
        return appReducer(action.payload, action);
      }

      return appReducer(state, action);
    };
  };

  return Object.freeze({
    makeRootReducer,
  });
};

export default reduxSwitch;
